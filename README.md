# Culendar
Culendar is a [free/libre](LICENSE) calendar application in python and
curses.

## Features
- Displays your events on a week, hour by hour
- Displays a TODO list
- Adds, deletes, edits, categorizes events
- Colour themes for your categorized events
- Imports from ICalendar format or from [calcurse](http://calcurse.org/)
  files
- Customizable
- Displays webcals
- Deals with caldavs

## How to install
- Download it, run it.
- Requires Python3 modules curses, datetime, icalendar, locale, gettext,
  caldav

## How to use
- Run it. Default key "h" to get help, "c" to configure.

## No warranties
Culendar has been tested on Debian stable and old-stable,
rxvt-unicode as a terminal and UTF-8 encoding only.
It needs much more testing to be officially reliable.

Don't hesitate to report bugs or request features.

## Screenshots
![culendar screenshot](screenshot0.png)

## Chat:
IRC: #culendar on irc.geeknode.org/6697
